import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home'

import Houses from '../views/houses/Index'
import House from '../views/houses/Show'
import CreateHouse from '../views/houses/Create'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/houses',
    name: 'houses',
    component: Houses
  },
  {
    path: '/houses/:id',
    name: 'houses.show',
    component: House
  },
  {
    path: '/create/houses',
    name: 'houses.create',
    component: CreateHouse
  }
]

const router = new VueRouter({
  routes
})

export default router
