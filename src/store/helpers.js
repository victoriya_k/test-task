import Vue from 'vue'

import _each from 'lodash/each'
import _findIndex from 'lodash/findIndex'
import _get from 'lodash/get'

export const addOrUpdateItems = (state, items, key = '_id', node = 'items') => {
  _each(items, (item) => addOrUpdateItem(state, item, key, node))
}

export const addOrUpdateItem = (state, item, key = '_id', node = 'items') => {
  if (_findIndex(state[node], existing => _get(existing, key) === _get(item, key)) > -1) {
    Vue.set(state, node, [...state[node].filter(i => _get(i, key) !== _get(item, key)), item])
  } else {
    Vue.set(state, node, [...state[node], item])
  }
}

export const removeItem = (state, id, key = '_id', node = 'items') => {
  const index = _findIndex(state[node], existing => existing[key] === id)
  if (index > -1) {
    state[node].splice(index, 1)
    Vue.set(state, node, state[node])
  }
}
