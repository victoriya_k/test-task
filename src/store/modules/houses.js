import axios from 'axios'
import * as types from '../mutation-types'
import { addOrUpdateItems, removeItem } from '../helpers'

const state = {
  items: []
}

const getters = {
  all: state => state.items,
  getById: state => id => state.items.find(item => item._id === id)
}

const mutations = {
  [types.BATCH_HOUSES]: (state, items) => addOrUpdateItems(state, items)
}

const actions = {
  fetch ({ commit }) {
    return axios.get('/houses')
      .then(res => commit(types.BATCH_HOUSES, res.data.houses))
  },
  fetchById ({ commit }, id) {
    return axios.get(`/houses/${id}`)
      .then(res => commit(types.BATCH_HOUSES, [res.data]))
  },

  create ({ commit }, payload) {
    return axios.post('/houses', payload)
      .then(({ data }) => {
        commit(types.BATCH_HOUSES, [data])
        return data
      })
  },
  deleteById ({ commit }, id) {
    return axios.delete(`/houses/${id}`)
      .then(() => {
        removeItem(state, id)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
