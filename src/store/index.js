import Vue from 'vue'
import Vuex from 'vuex'

import Houses from './modules/houses'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    houses: Houses
  }
})
